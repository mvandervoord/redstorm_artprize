import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.Image;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.awt.AlphaComposite;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.io.IOException;
import java.net.*;

import org.opencv.core.Point;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

class Surface extends JPanel
        implements ActionListener {

    //Canvas (dimensions overridden by constructor)
    public static int WIDTH = 10;
    public static int HEIGHT = 10;
    public static int SQUARE = 10;
    public static int OFFSETX = 0;
    public static int OFFSETY = 0;
    private static double ELLIPSE_SIZE = 8;

    //Timing
    private final int DELAY = 30;
    private final int INITIAL_DELAY = 150;

    //Ellipses
    private final int ALPHA = 0x70;
    private final int NUMBER_OF_ELLIPSES = 40;
    private double GROW_THICK_SIZE = 0.05;
    private double GROW_DIAMETER_SIZE = 1;

    //Painting
    private static final int METHOD_SOLID_1 = 0;
    private static final int METHOD_SOLID_2 = 1;
    private static final int METHOD_SOLID_3 = 2;
    private static final int METHOD_SOLID_4 = 3;
    private static final int METHOD_SOLID_5 = 4;
    private static final int METHOD_SOLID_6 = 5;
    private static final int METHOD_RAINBOW = 6;
    private static final int NUM_METHODS = 7;

    private static final int EFFECT_NONE = 0;
    private static final int EFFECT_BUBBLES = 1;
    private static final int NUM_EFFECTS = 2;

    private static boolean resetting = false;
    private static boolean painting = true;
    private static int method = METHOD_RAINBOW;
    private static int effect = EFFECT_NONE; //EFFECT_BUBBLES;
    private static int paintPart = 85;
    private static Color paint;
    private static int options; //0 = message, 1 = vision, 2 = hide_camera, 4 = rotate90, 8 = rotate270, 16 = gray

    private final Color colors[] = {
        new Color(0x20, 0x20, 0x20, ALPHA),
        new Color(0x40, 0x40, 0x40, ALPHA),
        new Color(0x60, 0x60, 0x60, ALPHA),
        new Color(0x80, 0x80, 0x80, ALPHA),
        new Color(0xA0, 0xA0, 0xA0, ALPHA),
        new Color(0xC0, 0xC0, 0xC0, ALPHA),
        new Color(0xE0, 0xE0, 0xE0, ALPHA),
        new Color(0x20, 0x20, 0x60, ALPHA),
        new Color(0x40, 0x40, 0x80, ALPHA),
        new Color(0x60, 0x60, 0xA0, ALPHA),
        new Color(0x80, 0x80, 0xC0, ALPHA),
        new Color(0xA0, 0xA0, 0xE0, ALPHA),
        new Color(0xC0, 0xC0, 0xFF, ALPHA),
    };

    private final Color paintColors[] = {
        new Color(0xF0, 0x20, 0x20, ALPHA),
        new Color(0x20, 0xF0, 0x20, ALPHA),
        new Color(0x20, 0x20, 0xF0, ALPHA),
        new Color(0xF0, 0xF0, 0x20, ALPHA),
        new Color(0x20, 0xF0, 0xF0, ALPHA),
        new Color(0xF0, 0x20, 0xF0, ALPHA),
    };

    private VideoCapture camera;
    private BufferedImage img;
    private Rectangle2D.Float rectangle;
    private Ellipse2D.Float[] ellipses;
    private double esize[];
    private float estroke[];
    private Timer timer;

    static private boolean findingBlue = false;
    static private double posX = -1;
    static private double posY = -1;

    static private Mat warpMat;
    static private int rotate = 0;

    public Surface(int id, int w, int h, int opts) {

        WIDTH = w;
        HEIGHT = h;
        SQUARE = (w > h) ? h : w;
        OFFSETX = (WIDTH - SQUARE) / 2;
        OFFSETY = (HEIGHT - SQUARE) / 2;

        ELLIPSE_SIZE = WIDTH / 8.0;

        posX = OFFSETX;
        posY = OFFSETY;

        options = opts;

        initSurface();
        initImage();
        initEllipses();
        initRectangle();
        initCamera(id);
        initTimer();
    }

    static public void setPos(double x, double y)
    {
        posX = OFFSETX + (SQUARE * x / 100.0);
        posY = OFFSETY + (SQUARE * y / 100.0);
    }

    static public void setButtons(String[] buttons)
    {
        for (int i=0; i < buttons.length; i++)
        {
            String btn = buttons[i].toUpperCase();
            if (btn.equals("A")) {
                System.out.println("Painting");
                painting = true;
            }
            else if (btn.equals("B")) {
                System.out.println("Not Painting");
                painting = false;
            }
            else if (btn.equals("X")) {
                System.out.println("Color");
                method = (method + 1) % NUM_METHODS;
            }
            else if (btn.equals("Y")) {
                System.out.println("Effect");
                effect = (effect + 1) % NUM_EFFECTS;
            }
            else if (btn.equals("C")) {
                System.out.println("Clear");
                resetting = true;
            }
            else if (btn.equals("S")) {
                System.out.println("Select Robot");
                findingBlue = !findingBlue;
                resetting = true;
            }
        }
    }

/////////////////////////////////// INITIALIZATION //////////////////////////////////

    private void initImage() {

        img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);

        //Create plan to Correct Trapezoid
        MatOfPoint2f src = new MatOfPoint2f(
            new Point(30, 0),
            new Point(WIDTH-30, 0),
            new Point(0, HEIGHT-1),
            new Point(WIDTH-1, HEIGHT-1) );
        MatOfPoint2f dst = new MatOfPoint2f(
            new Point(0, 0),
            new Point(WIDTH-1, 0),
            new Point(0, HEIGHT-1),
            new Point(WIDTH-1, HEIGHT-1) );
        Mat warpMat = Imgproc.getPerspectiveTransform(src,dst);

        resetImage();
    }

    private void resetImage() {

        Graphics2D g2 = img.createGraphics();
        g2.setColor(new Color(0x80, 0x80, 0x80, 0x80)); //Transparent
        g2.fillRect(0, 0, WIDTH, HEIGHT);
        g2.dispose();
    }

    private void initSurface() {

        setBackground(Color.black);
        ellipses = new Ellipse2D.Float[NUMBER_OF_ELLIPSES];
        esize = new double[ellipses.length];
        estroke = new float[ellipses.length];
        rectangle = new Rectangle2D.Float();
    }

    private void initRectangle() {

        rectangle.setFrame(100, 100, WIDTH * 2 / 19, HEIGHT * 2 / 19);
    }

    private void initEllipses() {

        for (int i = 0; i < ellipses.length; i++) {

            ellipses[i] = new Ellipse2D.Float();
            posRandEllipses(i, ELLIPSE_SIZE * Math.random());
        }
    }

    private void initCamera(int id) {

        camera = new VideoCapture(id);
    }

    private void initTimer() {

        timer = new Timer(DELAY, this);
        timer.setInitialDelay(INITIAL_DELAY);
        timer.start();
    }

///////////////////////////////// CALCULATIONS /////////////////////////////////////

    private void posRandEllipses(int i, double size) {

        esize[i] = size;
        estroke[i] = 1.0f;
        double x = Math.random() * (SQUARE - (ELLIPSE_SIZE / 2));
        double y = Math.random() * (SQUARE - (ELLIPSE_SIZE / 2));
        ellipses[i].setFrame(x + OFFSETX, y + OFFSETY, size, size);
    }

    private void doStep() {

        //Update Ellipses and Such
        for (int i = 0; i < ellipses.length; i++) {

            estroke[i] += GROW_THICK_SIZE;
            esize[i] += GROW_DIAMETER_SIZE;

            if (esize[i] > ELLIPSE_SIZE) {

                posRandEllipses(i, 1);
            } else {

                ellipses[i].setFrame(ellipses[i].getX() - 0.5, ellipses[i].getY() - 0.5, esize[i], esize[i]);
            }
        }

        //Update Paint Color
        if (method == METHOD_RAINBOW)
        {
            paintPart++;
            int p1 = (int)((Math.sin((float)(paintPart + 0)   / 60.0) + 1) * 127.0) & 0x000000FF;
            int p2 = (int)((Math.sin((float)(paintPart + 120) / 60.0) + 1) * 127.0) & 0x000000FF;
            int p3 = (int)((Math.sin((float)(paintPart + 240) / 60.0) + 1) * 127.0) & 0x000000FF;
            paint = new Color(p1, p2, p3, 0x80);
        }
        else
        {
            paint = paintColors[method];
        }

        //Reset if Requested
        if (resetting)
        {
            resetting = false;
            resetImage();
        }

        //Actually Update the painting
        updatePosition();
        if (painting)
        {
            Graphics2D g2 = img.createGraphics();
            g2.setColor(paint);
            g2.fillRect((int)rectangle.x, (int)rectangle.y, (int)rectangle.width, (int)rectangle.height);
            g2.dispose();
        }
    }

    public void updatePosition() {
        //scale rect to the window size
        Dimension dim = this.getSize();
        if ((options & 1) == 0)
        {
            rectangle.x = (float)(posX);
            rectangle.y = (float)(posY);
        }
        else
        {
            Point pt = findCenterOfRobot();
            rectangle.x = (float)(pt.x);
            rectangle.y = (float)(pt.y);
        }
        rectangle.width = SQUARE * 2 / 19;
        rectangle.height = SQUARE * 2 / 19;
        rectangle.setFrame(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

/////////////////////// DRAWING ////////////////////////////////////////////////////

    private void drawBackground(Graphics2D g2d) {

        try
        {
            if ((options & 2) == 0)
            {
                Mat raw = new Mat();
                Mat frame = new Mat();
                camera.read(raw);

                if ((options & 4) == 4)
                {
                    Core.transpose(raw, frame);
                    Core.flip(frame, frame, 1);
                }
                else if ((options & 8) == 8)
                {
                    Core.transpose(raw, frame);
                    Core.flip(frame, frame, 0);
                }
                else
                {
                    frame = raw;
                }

                //Mat corrected_frame = new Mat();
                //Imgproc.warpPerspective(frame, corrected_frame, warpMat, frame.size());

                int type = 0;
                if (frame.channels() == 1) {
                    type = BufferedImage.TYPE_BYTE_GRAY;
                } else if (frame.channels() == 3) {
                    type = BufferedImage.TYPE_3BYTE_BGR;
                }
                BufferedImage image = new BufferedImage(frame.width(), frame.height(), type);
                WritableRaster raster = image.getRaster();
                DataBufferByte dataBuffer = (DataBufferByte)raster.getDataBuffer();
                byte[] data = dataBuffer.getData();
                frame.get(0, 0, data);

                g2d.drawImage(image, 0, 0, WIDTH, HEIGHT, null);
            }
        }
        catch(Exception e) {}
    }

    private void drawImage(Graphics2D g2d) {

        g2d.setComposite(AlphaComposite.SrcOver.derive((256 - ALPHA) / 256.0f));
        g2d.drawImage(img, 0, 0, WIDTH, HEIGHT, null);
    }

    private Point center_pt;

    private Point findCenterOfRobot() {

        if (center_pt == null)
            center_pt = new Point(0,0);

        double px = 0;
        double py = 0;
        double total = 0;
        int size = 200;
                                         //H 0-180  S255 V255
        final Scalar redLower = new Scalar(0   / 2, 100, 100  );
        final Scalar redUpper = new Scalar(25  / 2, 255, 255 );
        final Scalar bluLower = new Scalar(215 / 2, 150, 150 );
        final Scalar bluUpper = new Scalar(245 / 2, 255, 255 );

        Mat raw   = new Mat();
        Mat frame = new Mat();
        Mat resiz = new Mat();
        Mat blurd = new Mat();
        Mat cnvrt = new Mat();
        Mat roi   = new Mat();
        Mat junk  = new Mat();
        Mat anchr = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1,1));

        camera.read(raw);

        if ((options & 4) == 4)
        {
            Core.transpose(raw, frame);
            Core.flip(frame, frame, 1);
        }
        else if ((options & 8) == 8)
        {
            Core.transpose(raw, frame);
            Core.flip(frame, frame, 0);
        }
        else
        {
            frame = raw;
        }

        Imgproc.resize( frame, resiz, new Size(size, size) );
        Imgproc.GaussianBlur( resiz, blurd, new Size(7, 7), 50 );
        if ((options & 16) == 0)
        {
            Imgproc.cvtColor( blurd, cnvrt, Imgproc.COLOR_BGR2HSV );
            Imgproc.erode(  cnvrt, cnvrt, anchr, new Point(-1,-1), 3 );
            Imgproc.dilate( cnvrt, cnvrt, anchr, new Point(-1,-1), 3 );
            if (findingBlue)
                Core.inRange( cnvrt, bluLower, bluUpper, cnvrt );
            else
                Core.inRange( cnvrt, redLower, redUpper, cnvrt );
        }
        else
        {
            Imgproc.erode(  cnvrt, cnvrt, anchr, new Point(-1,-1), 3 );
            Imgproc.dilate( cnvrt, cnvrt, anchr, new Point(-1,-1), 3 );
            Imgproc.cvtColor( blurd, cnvrt, Imgproc.COLOR_BGR2GRAY );
            Core.inRange( cnvrt, new Scalar(0), new Scalar(40), cnvrt );
        }

        java.util.List<MatOfPoint> contours = new java.util.ArrayList<MatOfPoint>();
        Imgproc.findContours(cnvrt, contours, junk, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        for (int i=0; i < contours.size(); i++) {
            Point center = new Point();
            float[] radius = new float[1];
            Imgproc.minEnclosingCircle(new MatOfPoint2f(contours.get(i).toArray()), center, radius);
            if ((radius[0] > 5) && (radius[0] < 30)) {
                center.x = center.x * WIDTH / size;
                center.y = center.y * HEIGHT / size;
                center_pt = center;
            }
        }

        return center_pt;
    }

    private void drawEllipses(Graphics2D g2d) {

        for (int i = 0; i < ellipses.length; i++) {

            g2d.setColor(colors[i % colors.length]);
            g2d.setStroke(new BasicStroke(estroke[i]));
            g2d.draw(ellipses[i]);
        }
    }

    private void drawRectangle(Graphics2D g2d) {

        Color rectcolor = new Color(paint.getRed() | 0x44, paint.getGreen() | 0x44, paint.getBlue() | 0x44);
        g2d.setColor(rectcolor);
        g2d.setStroke(new BasicStroke(2.0f));
        g2d.draw(rectangle);

        Rectangle2D.Float targetRect = new Rectangle2D.Float(rectangle.x+2, rectangle.y+2, rectangle.width-4, rectangle.height-4);
        Color targetColor = new Color(255,0,0);
        if (findingBlue)
            targetColor = new Color(0,0,255);
        g2d.setColor(targetColor);
        g2d.setStroke(new BasicStroke(2.0f));
        g2d.draw(targetRect);
    }

    private void doDrawing(Graphics g) {

        Graphics2D g2d = (Graphics2D) g.create();

        RenderingHints rh
                = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);

        rh.put(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        g2d.setRenderingHints(rh);

        Dimension size = getSize();
        doStep();

        drawBackground(g2d);
        drawImage(g2d);

        if (effect == EFFECT_BUBBLES)
            drawEllipses(g2d);

        drawRectangle(g2d);

        g2d.dispose();
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        repaint();
    }
}

///////////////////////// LISTENER /////////////////////////////////////
class Listener implements Runnable {

    public void run() {

        try {
            DatagramSocket serverSocket = new DatagramSocket(7654);
            byte[] receiveData = new byte[256];

            System.out.printf("Listening on udp:%s:%d%n", InetAddress.getLocalHost().getHostAddress(), 7654);
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

            while(true)
            {
                serverSocket.receive(receivePacket);
                String sentence = new String( receivePacket.getData(), 0, receivePacket.getLength() );
                //System.out.println("RECEIVED: " + sentence);
                if (sentence.substring(0,7).equals("RSR_XY:"))
                {
                    //System.out.println("XY: " + sentence.substring(7));
                    String[] parts = sentence.substring(7).split(",");
                    float x = Float.parseFloat(parts[0]);
                    float y = Float.parseFloat(parts[1]);
                    Surface.setPos(x,y);
                }
                else if (sentence.substring(0,8).equals("RSR_BTN:"))
                {
                    //System.out.println("Button: " + sentence.substring(8));
                    String[] buttons = sentence.substring(8).split(",");
                    Surface.setButtons(buttons);
                }
            }
        }
        catch (IOException e)
        {
            System.out.println(e);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
public class DrawTest extends JFrame {

    //public final int WIDTH = 1920;
    //public final int HEIGHT = 1080;
    //public final int WIDTH = 1920 / 2;
    //public final int HEIGHT = 1080 / 2;
    public final int WIDTH = 1080;
    public final int HEIGHT = 720;
    public static int id;
    public static int display = 0;
    public static int options;
    public Surface surface;

    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

    public DrawTest() {

        initUI();
    }

    private void initUI() {

        surface = new Surface(id, WIDTH, HEIGHT, options);
        add(surface);

        setTitle("Red Storm Robotics LOVES ArtPrize");
        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setUndecorated(true);

        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] myDevice = env.getScreenDevices();
        if (myDevice.length > display)
            display = 0;
        try {
            myDevice[display].setFullScreenWindow(this);
        }
        finally {
            myDevice[display].setFullScreenWindow(null);
        }
    }

    public static void main(String[] args) {

        //Check arguments
        id = 0;
        options = 0;
        for(int a = 0; a < args.length; a++) {
            String arg = args[a];
            for (int i = 0; i < arg.length(); i++)
            {
                char c = arg.charAt(i);
                switch(c)
                {
                    case 'L':
                        System.out.println("LISTENING FOR X,Y COORDINATES");
                        options &= 0xFFFFFFFE;
                        break;

                    case 'V':
                        System.out.println("VISION DETECTING X,Y COORDINATES");
                        options |= 1;
                        break;

                    case 'N':
                        System.out.println("NO CAMERA SHOWN");
                        options |= 2;
                        break;

                    case '+':
                        System.out.println("ROTATE 90");
                        options |= 4;
                        break;

                    case '-':
                        System.out.println("ROTATE 270");
                        options |= 8;
                        break;

                    case 'G':
                        System.out.println("GRAY COLOR MATCHING");
                        options |= 16;
                        break;

                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                      id = (int)(c - '0');
                      break;

                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                      display = (int)(c - 'a');
                      break;

                }
            }
        }

        //Start the Animation Routines
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                DrawTest ex = new DrawTest();
                ex.setVisible(true);
            }
        });

        //Start the UDP Listener
        Listener listener = new Listener();
        Thread t1 = new Thread(listener);
        t1.start();
    }
}
