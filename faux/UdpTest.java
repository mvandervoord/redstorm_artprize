import java.io.*;
import java.net.*;
import java.util.concurrent.TimeUnit;
import java.io.InputStreamReader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

class UdpTest
{
   public UdpTest()
   {
      JFrame frame = new JFrame();

     //make sure the program exits when the frame closes
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.setTitle("RedStorm Robotics Art Control Console");
     frame.setSize(300,250);

     //This will center the JFrame in the middle of the screen
     frame.setLocationRelativeTo(null);
     frame.getContentPane().setLayout(new GridLayout(6,1));

     //------------------------------------
     JButton btnPaint = new JButton("Paint");
     btnPaint.addActionListener(new ActionListener()
     {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            String sentence = "RSR_BTN:A";
            byte[] sendData = sentence.getBytes();
            try
            {
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
            }
            catch(java.io.IOException e) { }
         }
     });
     frame.add(btnPaint);

     //------------------------------------
     JButton btnDoNotPaint = new JButton("Do Not Paint");
     btnDoNotPaint.addActionListener(new ActionListener()
     {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            String sentence = "RSR_BTN:B";
            byte[] sendData = sentence.getBytes();
            try
            {
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
            }
            catch(java.io.IOException e) { }
         }
     });
     frame.add(btnDoNotPaint);

     //------------------------------------
     JButton btnColor= new JButton("Color");
     btnColor.addActionListener(new ActionListener()
     {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            String sentence = "RSR_BTN:X";
            byte[] sendData = sentence.getBytes();
            try
            {
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
            }
            catch(java.io.IOException e) { }
         }
     });
     frame.add(btnColor);

     //------------------------------------
     JButton btnEffect = new JButton("Effect");
     btnEffect.addActionListener(new ActionListener()
     {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            String sentence = "RSR_BTN:Y";
            byte[] sendData = sentence.getBytes();
            try
            {
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
            }
            catch(java.io.IOException e) { }
         }
     });
     frame.add(btnEffect);

     //------------------------------------
     JButton btnClear = new JButton("Clear");
     btnClear.addActionListener(new ActionListener()
     {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            String sentence = "RSR_BTN:C";
            byte[] sendData = sentence.getBytes();
            try
            {
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
            }
            catch(java.io.IOException e) { }
         }
     });
     frame.add(btnClear);

     //------------------------------------
     JButton btnSelect = new JButton("Select");
     btnSelect.addActionListener(new ActionListener()
     {
         @Override
         public void actionPerformed(ActionEvent event)
         {
            String sentence = "RSR_BTN:S";
            byte[] sendData = sentence.getBytes();
            try
            {
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
            }
            catch(java.io.IOException e) { }
         }
     });
     frame.add(btnSelect);

     frame.setVisible(true);
   }

   private static InputStreamReader br;
   private static DatagramSocket clientSocket;
   private static InetAddress IPAddress;

   public static void main(String args[]) throws Exception
   {
      br = new InputStreamReader(System.in);
      clientSocket = new DatagramSocket();
      IPAddress = InetAddress.getByName("localhost");
      byte[] sendData = new byte[1024];
      byte[] receiveData = new byte[1024];
      int x = 0;
      int y = 0;

      new UdpTest();

      boolean keymode = ((args.length > 0) && (args[0].equals("keys")));

      if (keymode)
      {
         System.out.println("Press Buttons To Send");
         System.out.println("A - Paint");
         System.out.println("B - Do Not Paint");
         System.out.println("X - Color");
         System.out.println("Y - Effect");
         System.out.println("C - Clear Surface");
         System.out.println("S - Select Robot");
      }

      try
      {
         while(true)
         {
            if (!keymode) {
               x = (x + 3) % 1000;
               y = (y + 7) % 1000;
               String sentence = "RSR_XY:" + Float.toString((float)x / 10.0f) + "," + Float.toString((float)y / 10.0f);
               sendData = sentence.getBytes();
               DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
               clientSocket.send(sendPacket);
               System.out.print(".");
               //System.out.println(sentence);
            }
            else {
               int b = br.read();
               if (b > 0)
               {
                  String sentence = "RSR_BTN:" + (char)b;
                  sendData = sentence.getBytes();
                  DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7654);
                  clientSocket.send(sendPacket);
                  System.out.print("[" + (char)b + "]");
               }
            }

            TimeUnit.MILLISECONDS.sleep(100);
         }
      }
      finally {
          clientSocket.close();
      }
   }
}
