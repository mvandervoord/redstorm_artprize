if (RUBY_PLATFORM =~ /mswin/)
  registry_key "HKEY_CURRENT_USER\\Environment" do
    $path_name = ""
    subkey_array = registry_get_values("HKEY_CURRENT_USER\\Environment", :x86_64)
    subkey_array.each{ |val|
      case val[:name].include?("PATH")
      when true
          $path_name = val[:data]
          print "\n The User PATH is: #{$path_name}"
          break
      when false
        print ':'
      end
    }
      values [{
        :name => "PATH",
        :type => :string,
        :data => "#{$path_name};C:\\ProgramData\\Oracle\\Java\\javapath;C:\\Progra~1\\Java\\jdk1.8.0_141\\bin;C:\\apache-ant-1.10.1\\bin"
        }]
    action :create
    #add a guard to prevent duplicates
    not_if {
        $path_name.include?("C:\\apache-ant-1.10.1")
    }
  end
end

$opts = "0L"

task :default => [ :run ]

desc "Select Camera Device"
task :device, [:dev] do |t, args|
  $opts = $opts.gsub(/\d/,"") + args[:dev].to_s
end

desc "Select Display Device"
task :display, [:dev] do |t, args|
  $opts = $opts.gsub(/[abcde]/,"") + ["a","b","c","d","e"][args[:dev].to_i]
end

desc "Use external Tracking"
task :listen do
  $opts = $opts.gsub(/[VL]/,"") + "L"
end

desc "Use vision to Track"
task :vision do
  $opts = $opts.gsub(/[VL]/,"") + "V"
end

desc "Do Not Show Camera"
task :nocam do
  $opts = $opts.gsub(/[N]/,"") + "N"
end

desc "Rotate 90"
task :rot90 do
  $opts = $opts.gsub(/[\+]/,"") + "+"
end

desc "Rotate 270"
task :rot270 do
  $opts = $opts.gsub(/[\-]/,"") + "-"
end

desc "Use Gray Instead of Color Matching"
task :gray do
  $opts = $opts.gsub(/[G]/,"") + "G"
end

desc "Build and Run"
task :run do
  puts $opts.inspect
  puts `ant -DocvJarDir=../opencv/build/bin -DocvLibDir=../opencv/build/lib -Doptions=#{$opts}`
end

desc "Build Control App"
task :ctrl do
  Dir.chdir("faux") {
    puts `javac UdpTest.java && java UdpTest keys`
  }
end

